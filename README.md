# This is a fork !

This project is a fork of [Stav Shamir](https://github.com/stavshamir/docker-tutorial) adapted for a course purpose

According to the original README.md, this project refers to [blog post about creating a flask-mysql app with docker](https://stavshamir.github.io/python/dockerizing-a-flask-mysql-app-with-docker-compose/)

## GitLab CI

It's not the original file. I adapt it for course purpose. Feel free to get more information on the original GitHub Account
